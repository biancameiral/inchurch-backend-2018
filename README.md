# inChurch Recruitment Process 2018 - Backend Developer #

>>Install Django 2.1.5, Python 3.7.2, Pillow
>>Run makemigrations and migrate 


Then, access http://localhost:8000/ and check out login, sign up and departments page. If you sign up with an invalid username, it'll be written in the page.
If you are an admin go to http://localhost:8000/admin, you can see the users and add more departments.

To have access to admin area:
	username: inchurch
	password: inchurch2019
	email: inchurch@gmail.com

You can do tests signing up new users and adding departments.
