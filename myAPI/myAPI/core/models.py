from django.db import models

# Create your models here.
class CourseManager(models.Manager):

	def search(self, query):
		return self.get_queryset().filter(
			models.Q(name__icontains=query) | \
			 models.Q(description__icontais=query)
			)

	class Meta:
		verbose_name ='Curso'
		verbose_name_plural = 'Cursos'
		ordering = ['name']