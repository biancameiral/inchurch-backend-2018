from django.contrib import admin, auth
from django.urls import path, include
from myAPI.core.views import home, password
from myAPI.courses.views import courses
from myAPI.accounts.views import accounts
from  myAPI.accounts import views

urlpatterns = [
    path('', home, name='home'),
    #path('password_reset_form', password, name='password'),
    path('admin/', admin.site.urls),
    path('departments/', courses, name='courses'),
    path('accounts/signup/', views.SignUp.as_view(), name='signup'),
    #path('accounts/', include('accounts.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
   
    
    #path('accounts/', include('allauth.urls')),
    #path('login/', accounts,  name='login'),
    #path('blog/', include('auth.views.login')),
  
   
]
